#ifndef IOBENCH_FUNCTIONS_HPP
#define IOBENCH_FUNCTIONS_HPP

#include "FSLib/utilities.hpp"
#include <string>
#include <vector>

enum parseResults {
    PARSE_OK,
    PARSE_HELP,
    PARSE_ERROR
};

void printHelp();

int parseCommandLine( int argc, char **argv, uint64_t &file_size,
                      uint64_t &file_count, uint64_t &creation_file_count,
                      int &threads, std::string &benchmarks, bool &ssh,
                      bool &delete_files, std::vector< std::string > &nodes,
                      std::string &path );

std::vector< std::string >
parseNodesFile( const std::string &file_path,
                const std::string &user = getCurrentPWD()->pw_name );

bool parseConfig( const std::string &file_path, uint64_t &file_count,
                  uint64_t &file_size, uint64_t &creation_file_count,
                  int &threads, bool &delete_files,
                  std::vector< std::string > &nodes, std::string &benchmarks,
                  std::string &path );

// reset log file to its initial state
// (only has "IOBENCH RUN LOG" in it
void resetLOG();

// start benchmark for network connected nodes
bool nodeBenchmark( std::string &benchmarks,
                    const std::vector< std::string > &nodes,
                    uint64_t &file_count, uint64_t &file_size,
                    uint64_t &creation_file_count, int &threads,
                    const bool &delete_files, const std::string &path );

// start benchmark locally
void localBenchmark( std::string &benchmarks, uint64_t &file_count,
                     uint64_t &file_size, uint64_t &creation_file_count,
                     int &threads, const bool &delete_files,
                     const std::string &path );

// run benchmark and report result to server node
void sshBenchmark( uint64_t &file_count, uint64_t &file_size,
                   uint64_t &creation_file_count, int &threads,
                   const bool &delete_files, const std::string &path );

#endif
