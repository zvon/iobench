#ifndef IONETWORK_H
#define IONETWORK_H

#include "ssh.hpp"
#include <memory>
#include <vector>

void start_server( const std::string &benchmarks,
                   std::vector< std::unique_ptr< SSH > > &ssh_connections );

#endif
