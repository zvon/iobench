// this has been based on tutorial on libssh.org

#ifndef SSH_H
#define SSH_H

#include <libssh/libssh.h>
#include <stdexcept>

#if LIBSSH_VERSION_MINOR < 8
#include <functional>
#endif

class SSH {
public:
    SSH() {
        session_ = ssh_new();
    }

    SSH( const std::string &hostname, int port ) : SSH() {
        if ( !assignAddress( hostname, port ) ) {
            throw std::runtime_error( "Couldn't start SSH session" );
        }
    }

    SSH( const std::string &hostname, int port, const std::string &user )
            : SSH( hostname, port ) {
        setUser( user );
    }

    ~SSH() {
        cleanUp();
    }

    // connect ssh on provided hostname and port
    bool assignAddress( const std::string &hostname, int port );

    // set configuration for ssh
    bool setConfig( const std::string &config );

    void setUser( const std::string &user );

    // start connection
    bool start();

    // authenticate using ssh key
    bool authenticateKey();

    // authenticate using password
    bool authenticatePassword();

    // execute given command on connected machine
    bool executeCommand( const std::string &command );

    // wait until connected machine responds, then read nonblockingly
    // and return output
    std::string commandOutputBlockOnce() const;

    // send message to machine's stdin
    void sendMessage( const char &c );

    // send message to machine's stdin
    void sendMessage( const std::string &str );

    void closeConnection();

private:
    ssh_session session_{ nullptr };
    ssh_channel channel_{ nullptr };
    int port_;
    std::string hostname_;
    std::string user_;

#if LIBSSH_VERSION_MINOR < 8
    // unify function/constant names

    std::function< int( ssh_session, ssh_key * ) > ssh_get_server_publickey =
        ssh_get_publickey;
    std::function< int( ssh_session ) > ssh_session_is_known_server =
        ssh_is_server_known;
    std::function< int( ssh_session ) > ssh_session_update_known_hosts =
        ssh_write_knownhost;

    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_OK =
        SSH_SERVER_KNOWN_OK;
    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_CHANGED =
        SSH_SERVER_KNOWN_CHANGED;
    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_OTHER =
        SSH_SERVER_FOUND_OTHER;
    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_NOT_FOUND =
        SSH_SERVER_FILE_NOT_FOUND;
    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_UNKNOWN =
        SSH_SERVER_NOT_KNOWN;
    static constexpr enum ssh_server_known_e SSH_KNOWN_HOSTS_ERROR =
        SSH_SERVER_ERROR;

#endif

    void cleanUp();

    // make sure host is known
    bool verifyHost();
};

#endif // SSH_H
