#include "ssh.hpp"
#include <iostream>
#include <string.h>

bool SSH::assignAddress( const std::string &hostname, int port ) {
    if ( session_ == nullptr )
        return false;
    hostname_ = hostname;
    port_ = port;
    ssh_options_set( session_, SSH_OPTIONS_HOST, hostname_.c_str() );
    ssh_options_set( session_, SSH_OPTIONS_PORT, &port_ );
    return true;
}

bool SSH::setConfig( const std::string &config ) {
    return ssh_options_parse_config( session_, config.c_str() ) == 0;
}

void SSH::setUser( const std::string &user ) {
    user_ = user;
}

bool SSH::start() {
    auto rc = ssh_connect( session_ );

    if ( rc != SSH_OK ) {
        std::cerr << "Error connecting to " << hostname_ << " "
                  << ssh_get_error( session_ ) << std::endl;
        return false;
    }

    // only continue if host is trusted
    if ( !verifyHost() )
        return false;

    channel_ = ssh_channel_new( session_ );

    if ( channel_ == nullptr )
        return false;

    return true;
}

bool SSH::authenticateKey() {
    auto rc = ssh_userauth_publickey_auto( session_, user_.c_str(), nullptr );

    if ( rc == SSH_AUTH_ERROR ) {
        std::cerr << "Authentication failed: " << ssh_get_error( session_ )
                  << std::endl;
        return false;
    }

    rc = ssh_channel_open_session( channel_ );

    if ( rc != SSH_OK ) {
        std::cerr << ssh_get_error( session_ ) << std::endl;
        return false;
    }

    return true;
}

bool SSH::authenticatePassword() {
    auto *pass = getpass( "Password: " );
    auto rc = ssh_userauth_password( session_, user_.c_str(), pass );

    if ( rc != SSH_AUTH_SUCCESS ) {
        std::cerr << "Error authenticating with password: "
                  << ssh_get_error( session_ ) << std::endl;
        return false;
    }

    rc = ssh_channel_open_session( channel_ );

    if ( rc != SSH_OK ) {
        std::cerr << ssh_get_error( session_ ) << std::endl;
        return false;
    }

    return true;
}

bool SSH::executeCommand( const std::string &command ) {
    auto rc = ssh_channel_request_exec( channel_, command.c_str() );
    if ( rc != SSH_OK )
        return false;
    return true;
}

std::string SSH::commandOutputBlockOnce() const {
    std::string out;
    char buffer[256];
    auto nbytes = ssh_channel_read( channel_, buffer, 255, 0 );
    buffer[nbytes] = '\0';

    while ( nbytes > 0 ) {
        out += buffer;
        nbytes = ssh_channel_read_nonblocking( channel_, buffer, 255, 0 );
        buffer[nbytes] = '\0';
    }

    return out;
}

void SSH::sendMessage( const char &c ) {
    char tmp[2];
    tmp[0] = c;
    tmp[1] = '\n';
    ssh_channel_write( channel_, tmp, 2 );
}

void SSH::sendMessage( const std::string &str ) {
    ssh_channel_write( channel_, str.c_str(), str.size() );
    if ( str.back() != '\n' ) {
        ssh_channel_write( channel_, "\n", 1 );
    }
}

void SSH::closeConnection() {
    cleanUp();
}

void SSH::cleanUp() {
    if ( channel_ != nullptr ) {
        ssh_channel_close( channel_ );
        ssh_channel_free( channel_ );
        channel_ = nullptr;
    }

    if ( session_ != nullptr ) {
        ssh_disconnect( session_ );
        ssh_free( session_ );
        session_ = nullptr;
    }
}

bool SSH::verifyHost() {
    unsigned char *hash = nullptr;
    ssh_key srv_pubkey = nullptr;
    size_t hlen;
    std::string input;
    char *hexa;

    auto rc = ssh_get_server_publickey( session_, &srv_pubkey );

    if ( rc < 0 ) {
        return false;
    }

    rc = ssh_get_publickey_hash( srv_pubkey, SSH_PUBLICKEY_HASH_SHA1, &hash,
                                 &hlen );
    ssh_key_free( srv_pubkey );

    if ( rc < 0 ) {
        return false;
    }

    auto state = ssh_session_is_known_server( session_ );

    switch ( state ) {
    case SSH_KNOWN_HOSTS_OK:
        // OK
        break;
    case SSH_KNOWN_HOSTS_CHANGED:
        std::cerr << "Host for server changed, it is now: ";
        ssh_print_hexa( "Public key hash", hash, hlen );
        std::cerr << "Please fix this issue before continuing" << std::endl;
        return false;
    case SSH_KNOWN_HOSTS_OTHER:
        std::cerr << "The host key for this server was not found" << std::endl;
        std::cerr << "Please fix this issue before continuing" << std::endl;
        return false;
    case SSH_KNOWN_HOSTS_NOT_FOUND:
        std::cerr << "Couldn't find known hosts file" << std::endl;
        std::cerr << "If you accept the host key here, the file will be "
                     "created automatically"
                  << std::endl;
        // fall through to hosts_unknown
    case SSH_KNOWN_HOSTS_UNKNOWN:
        hexa = ssh_get_hexa( hash, hlen );
        std::cout << "The server is unknown. Do you trust the host key?"
                  << std::endl;
        std::cout << "Public key hash: " << hexa << std::endl;

        ssh_string_free_char( hexa );
        ssh_clean_pubkey_hash( &hash );

        std::cin >> input;

        if ( input[0] != 'y' && input[0] != 'Y' )
            return false;

        rc = ssh_session_update_known_hosts( session_ );

        if ( rc < 0 ) {
            std::cerr << "Error " << strerror( errno ) << std::endl;
            return false;
        }
        break;
    case SSH_KNOWN_HOSTS_ERROR:
        std::cerr << "Error " << ssh_get_error( session_ ) << std::endl;
        ssh_clean_pubkey_hash( &hash );
        return false;
    }
    ssh_clean_pubkey_hash( &hash );
    return true;
}
