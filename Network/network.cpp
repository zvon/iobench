#include "network.hpp"
#include "../FSLib/utilities.hpp"
#include <iostream>
#include <numeric>

void start_server( const std::string &benchmarks,
                   std::vector< std::unique_ptr< SSH > > &ssh_connections ) {
    std::vector< std::vector< long double > > results;

    // this will block this thread until all ssh connections report
    // that they are online
    for ( const auto &x : ssh_connections ) {
        x->commandOutputBlockOnce();
    }

    // tell every node that they can start the benchmark now
    for ( uint64_t i = 0; i < benchmarks.size(); i++ ) {
        results.resize( ssh_connections.size() );
        for ( auto &x : ssh_connections )
            x->sendMessage( benchmarks[i] );

        // BlockOnce will block this thread until it gets a message
        // from ssh
        for ( unsigned long j = 0; j < ssh_connections.size(); j++ ) {
            auto response = ssh_connections[j]->commandOutputBlockOnce();
            if( response.empty() ) {
                i = benchmarks.size(); // end outside for loop
                std::cerr << "Something went wrong, check iobench_run.log"
                          << std::endl
                          << std::endl;
                break;
            }
            for ( auto &x : splitString( response, ',' ) ) {
                if ( !isdigit( x[0] ) ) {
                    i = benchmarks.size(); // end outside for loop
                    std::cerr << "Something went wrong, check iobench_run.log"
                              << std::endl
                              << std::endl;
                    break;
                }
                results[j].push_back( std::stold( x ) );
            }
        }

        if ( i == benchmarks.size() )
            break;

        switch ( benchmarks[i] ) {
        case 'w':
            LOG( "Server got these results for 'write':" );
            break;
        case 'r':
            LOG( "Server got these results for 'read':" );
            break;
        case 'm':
            LOG( "Server got these results for 'move':" );
            break;
        case 'd':
            LOG( "Server got these results for 'delete':" );
            break;
        case 'c':
            LOG( "Server got these results for 'create':" );
            break;
        default:
            break;
        }

        if ( benchmarks[i] == 'w' || benchmarks[i] == 'r' ||
             benchmarks[i] == 'm' || benchmarks[i] == 'd' ||
             benchmarks[i] == 'c' ) {
            for ( auto &x : results ) {
                for ( auto &y : x )
                    LOG( "\t" + std::to_string( y ) );
            }
        } else {
            // ignore 'f' and '0' "benchmarks"
            results.clear();
            continue;
        }

        // print results for multi node benchmark
        printResults( benchmarks[i], results );

        results.clear();
    }

    std::cout << "\x1b[1A"; // move up one line (empty line caused by
                            // printResults)

    for ( auto &x : ssh_connections )
        x->sendMessage( 'k' );
}
