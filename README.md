# IOBench - I/O Benachamrking tool

I've written this program as part of my bachelor's thesis. It can measure these operations: read, write, move, delete, create

## Usage

### CLI Arguments:

`-h, --help` - Print help

#### FILE OPTIONS
`-c, --file-count` - Number of files
`-s, --file-size` - Size of files
`--creation-count` - Number of files for creation benchmark (default is 100000)
`--path` - Path where to run benchmark

#### BENCHMARK OPTIONS
`--threads` - Number of threads to use
`--nodes` - Name of configuration file for nodes to be used in benchmark
`--config` - Benchmark configuration file
`--block-size` - Size of read/write block
`--write` - Run 'write' benchmark
`--read` - Run 'read' benchmark
`--move` - Run 'move' benchmark
`--remove` - Run 'remove' benchmark
`--delete` - Run 'remove' benchmark
`--creation` - Run 'creation' benchmark

### Exemplary configuration file
    files 100
    size 1G
    creation-files 100K
    block-size 256K
    threads 4
    benchmarks write read move delete creation
    nodes nodes.conf
    node server-name:22 /usr/local/bin/iobench
    path /path/to/directory/for/benchmark

### Exemplary nodes configuration file
    server1 user /usr/local/bin/iobench
    server2:8822 /usr/local/bin/iobench

When specifying node, if you omit the user, user that started the benchmark will be used.
