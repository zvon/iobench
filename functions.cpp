#include "functions.hpp"
#include "FSLib/benchmark.hpp"
#include "FSLib/fslib.hpp"
#include "Network/network.hpp"
#include "Network/ssh.hpp"
#include <cfloat>
#include <chrono>
#include <fcntl.h>
#include <fstream>
#include <functional>
#include <getopt.h>
#include <iostream>
#include <memory>
#include <mutex>
#include <numeric>
#include <sstream>
#include <string.h>
#include <thread>

constexpr unsigned int node_parameters = 4;

// LOG with current time at the beginning of message
void LOG_time( const std::string &message ) {
    auto now = std::chrono::system_clock::to_time_t(
        std::chrono::system_clock::now() );
    char time[100];
    std::strftime( time, sizeof( time ), "%c", std::localtime( &now ) );
    LOG( std::string( time ) + " - " + message );
}

void truncFiles( const std::vector< std::vector< std::string > > &files,
                 const uint64_t &file_size, const std::string &path ) {
    for ( uint64_t i = 0; i < files.size(); i++ ) {
        auto file_path = path + "/thread" + std::to_string( i ) + "/";
        for ( const auto &file : files[i] ) {
            auto cur_file = file_path + file;
            if ( FSLib::exists( cur_file ) &&
                 FSLib::filesize( cur_file ) == file_size )
                continue;
            auto fd = open( cur_file.c_str(), O_CREAT | O_TRUNC, 0644 );
            close( fd );
        }
    }
}

// create `file_count` of files with `file_size` size in `path`
std::vector< std::string > prepareFiles( uint64_t file_count,
                                         uint64_t file_size,
                                         const std::string &path = "." ) {
    if ( file_count == 0 )
        return {};

    // if given directory doesn't exist, create it
    if ( !FSLib::exists( path ) ) {
        if ( !FSLib::createDirectoryFull( path ) ) {
            LOG_time( "Error creating directory " + path + ": " +
                      strerror( errno ) );
            return {};
        }
    }

    std::vector< std::string > files;
    files.reserve( file_count );

    if ( !FSLib::exists( path + "/file_0" ) ||
         FSLib::filesize( path + "/file_0" ) != file_size ) {
        FSLib::randomFile( path + "/file_0", file_size );
    }

    files.push_back( "file_0" );

    for ( uint64_t i = 1; i < file_count; i++ ) {
        auto name = path + "/file_" + std::to_string( i );
        if ( !FSLib::exists( name ) || FSLib::filesize( name ) != file_size ) {
            if ( FSLib::copyFile( path + "/file_0", name ) != 0 ) {
                LOG( "Error copying file " + path + "/file_0 to " + name +
                     ": " + strerror( errno ) );
                // returning empty vector indicates something went wrong
                return {};
            }
        }
        files.emplace_back( "file_" + std::to_string( i ) );
    }

    return files;
}

// prepareFiles but for multiple threads
std::vector< std::vector< std::string > >
prepareFiles( int thread_count, uint64_t file_count, uint64_t file_size,
              const std::string &path = "." ) {
    LOG_time( "Preparing files" );

    if ( file_count == 0 )
        return {};

    std::vector< std::vector< std::string > > files;
    std::vector< std::thread > threads;

    files.resize( thread_count );
    threads.reserve( thread_count - 1 );

    std::mutex fileMutex;
    bool failed{ false };

    for ( int i = 1; i < thread_count; i++ )
        threads.emplace_back( [&files, &fileMutex, &file_count, &file_size,
                               &path, &failed, i]() {
            auto files_local = prepareFiles(
                file_count, file_size, path + "/thread" + std::to_string( i ) );
            if ( files_local.empty() )
                failed = true;
            fileMutex.lock();
            files[i] = files_local;
            fileMutex.unlock();
        } );

    auto files_local = prepareFiles( file_count, file_size, path + "/thread0" );
    if ( files_local.empty() )
        failed = true;
    fileMutex.lock();
    files[0] = files_local;
    fileMutex.unlock();

    for ( auto &x : threads )
        x.join();

    if ( failed )
        return {};

    return files;
}

// generates filenames for 'write' benchmark
std::vector< std::vector< std::string > >
generateFileNames( int thread_count, uint64_t file_count,
                   const std::string &path = "." ) {
    LOG_time( "Generating filenames" );

    if ( file_count == 0 )
        return {};

    std::vector< std::vector< std::string > > files;
    files.resize( thread_count );

    for ( int i = 0; i < thread_count; i++ ) {
        auto thread_path = path + "/thread" + std::to_string( i );
        if ( !FSLib::exists( thread_path ) )
            FSLib::createDirectoryFull( thread_path );
        files[i].reserve( file_count );
        for ( uint64_t j = 0; j < file_count; j++ ) {
            files[i].emplace_back( "file_" + std::to_string( j ) );
        }
    }

    return files;
}

// run a multithreaded benchmark
std::vector< long double > multiThread(
    uint64_t file_size, std::vector< std::vector< std::string > > &files,
    const std::string &path = ".",
    std::function< long double( std::vector< std::string > &files,
                                uint64_t file_size, uint64_t file_count,
                                const std::string &path ) >
        bench = writeBenchmark,
    std::function< bool( const std::vector< std::string > &,
                         const std::string & ) >
        clean = noCleanUp,
    uint64_t file_count = 0 ) {
    std::vector< long double > results;
    std::vector< std::thread > threads;
    std::mutex readyMutex; // used to avoid writing results at the same time
    std::mutex benchMutex; // used to wait for all threads to be ready before
                           // starting a benchmark

    benchMutex.lock();
    auto thread_count = files.size();

    for ( uint64_t i = 1; i < thread_count; i++ )
        threads.emplace_back( [&files, &results, &file_size, &file_count,
                               &readyMutex, &benchMutex, &path, &bench, &clean,
                               i]() {
            auto &files_local = files[i];
            benchMutex.lock();
            benchMutex.unlock();
            // run benchmark
            auto result = bench( files_local, file_size, file_count,
                                 path + "/thread" + std::to_string( i ) );
            // store result
            readyMutex.lock();
            results.push_back( result );
            readyMutex.unlock();
            // clean up after benchmark
            clean( files_local, path + "/thread" + std::to_string( i ) );
        } );

    auto &files_local = files[0];
    benchMutex.unlock(); // start benchmarking

    // run benchmark
    auto result =
        bench( files_local, file_size, file_count, path + "/thread0" );
    // store result
    readyMutex.lock();
    results.push_back( result );
    readyMutex.unlock();
    // clean up after benchmark
    clean( files_local, path + "/thread0" );

    // wait until all threads have reported results
    while ( results.size() != thread_count )
        std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    for ( auto &x : threads ) {
        x.join();
    }

    for( const auto &x : results ) {
        if( x == LDBL_MAX )
            return {};
    }

    return results;
}

std::vector< long double >
multiThreadWrite( uint64_t file_size,
                  std::vector< std::vector< std::string > > &files,
                  const std::string &path = "." ) {
    LOG_time( "Starting write benchmark" );
    return multiThread( file_size, files, path, writeBenchmark );
}

std::vector< long double >
multiThreadRead( uint64_t file_size,
                 std::vector< std::vector< std::string > > &files,
                 const std::string &path = "." ) {
    LOG_time( "Starting read benchmark" );
    return multiThread( file_size, files, path, readBenchmark );
}

std::vector< long double >
multiThreadMove( uint64_t file_size,
                 std::vector< std::vector< std::string > > &files,
                 const std::string &path = "." ) {
    LOG_time( "Starting move benchmark" );
    return multiThread( file_size, files, path, moveBenchmark, cleanUpMoved );
}

std::vector< long double >
multiThreadRemove( uint64_t file_size,
                   std::vector< std::vector< std::string > > &files,
                   const std::string &path = "." ) {
    LOG_time( "Starting remove benchmark" );
    return multiThread( file_size, files, path, removeBenchmark );
}

// creation benchmark
std::vector< long double > multiThreadCreate( int threads, uint64_t file_count,
                                              const std::string &path = "." ) {
    LOG_time( "Starting creation benchmark" );

    // use a special directory for creation benchmark
    auto creation_path = path + "/creation";

    FSLib::createDirectoryFull( creation_path );
    for ( int i = 0; i < threads; i++ ) {
        FSLib::createDirectoryFull( creation_path + "/thread" +
                                    std::to_string( i ) );
    }

    std::vector< std::vector< std::string > > empty{};
    // need to resize to number of threads
    // because multiThread uses size to determine thread count
    empty.resize( threads );

    // run benchmark
    return multiThread( 0, empty, creation_path, creationBenchmark, noCleanUp,
                        file_count );
}

void multiCleanUpCreation( uint64_t file_count, int thread_count,
                           const std::string &path = "./creation" ) {
    std::vector< std::thread > threads;

    threads.reserve( thread_count - 1 );

    for ( int i = 1; i < thread_count; i++ ) {
        // LOG
        LOG_time( "Removing " + path + "/thread" + std::to_string( i ) );
        threads.emplace_back( [&file_count, &path, i]() {
            FSLib::cleanUpCreation( file_count,
                                    path + "/thread" + std::to_string( i ) );
            FSLib::remove( path + "/thread" + std::to_string( i ) );
        } );
    }

    LOG_time( "Removing " + path + "/thread0" );
    FSLib::cleanUpCreation( file_count, path + "/thread0" );
    FSLib::remove( path + "/thread0" );

    for ( auto &x : threads ) {
        x.join();
    }

    FSLib::remove( path );
}

// remove files used for benchmark
void multiCleanUp( const std::vector< std::vector< std::string > > &files,
                   const std::string &path = "." ) {
    std::vector< std::thread > threads;

    threads.reserve( files.size() - 1 );

    for ( uint64_t i = 1; i < files.size(); i++ ) {
        // LOG
        LOG_time( "Removing " + path + "/thread" + std::to_string( i ) );
        threads.emplace_back( [&files, &path, i]() {
            FSLib::cleanUp( files[i], path + "/thread" + std::to_string( i ) );
            FSLib::remove( path + "/thread" + std::to_string( i ) );
        } );
    }

    LOG_time( "Removing " + path + "/thread0" );
    FSLib::cleanUp( files[0], path + "/thread0" );
    FSLib::remove( path + "/thread0" );

    for ( auto &x : threads ) {
        x.join();
    }
}

// remove all 'thread*' directories, assumes directories are empty
void multiRemoveDirs( uint64_t thread_count, const std::string &path = "." ) {
    for ( uint64_t i = 0; i < thread_count; i++ ) {
        FSLib::remove( path + "/thread" + std::to_string( i ) );
    }
}

// run appropriate benchmark for given char
std::vector< long double >
runBenchmark( const char &benchmark,
              std::vector< std::vector< std::string > > &files,
              const uint64_t &file_size, const uint64_t &file_count,
              const uint64_t &creation_file_count, const int &threads,
              const std::string &path = "." ) {
    switch ( benchmark ) {
    case 'c':
        return multiThreadCreate( threads, creation_file_count, path );
    case 'm':
        return multiThreadMove( file_size, files, path );
    case 'r':
        return multiThreadRead( file_size, files, path );
    case 'd':
        return multiThreadRemove( file_size, files, path );
    case 'w':
        return multiThreadWrite( file_size, files, path );
        break;
    case 'f':
        files = prepareFiles( threads, file_count, file_size, path );
        if ( files.empty() )
            return {};
        break;
    case '0':
        files = generateFileNames( threads, file_count, path );
        if ( files.empty() )
            return {};
        truncFiles( files, file_size, path );
    default:
        break;
    }
    return { 0 };
}

// get last char of a char* string
char lastChar( const char *const string ) {
    auto pos = strlen( string );
    return string[pos - 1];
}

// create ssh connections on given nodes
bool startNodes( const std::vector< std::string > &nodes,
                 std::vector< std::unique_ptr< SSH > > &ssh_connections,
                 const uint64_t &file_count, const uint64_t &file_size,
                 const uint64_t &creation_count, const int &threads,
                 const bool &delete_files, const std::string &path ) {
    LOG_time( "Starting ssh clients" );

    auto cur_path = cwd();

    // wouldn't make sense to continue if cwd failed
    if ( cur_path == "." )
        return false;

    for ( uint64_t i = 0; i < nodes.size(); i += node_parameters ) {
        // get port number, if not specified default to 22
        int port = nodes[i + 3].empty() ? 22 : std::stoi( nodes[i + 3] );
        // create a new ssh connection
        ssh_connections.emplace_back( new SSH( nodes[i], port, nodes[i + 1] ) );

        if ( ssh_connections.back()->start() ) {
            // authenticate using ssh key
            if ( ssh_connections.back()->authenticateKey() ) {
                auto node_number = std::to_string( i / node_parameters );

                std::string final_command{};
                if( delete_files ) {
                    final_command = "; cd .. ; rm -Rf node_" + node_number;
                } else {
                    final_command = "";
                }

                // enter path, create node directory, enter it, write to log
                // file, start iobench with apropriate parameters, add log to
                // the end of main log file, remove node directory
                ssh_connections.back()->executeCommand(
                    "cd " + path + " && if [ ! -d node_" + node_number +
                    " ] ; then mkdir node_" + node_number + "; fi && cd node_" +
                    node_number + " && echo -e \"\\nNode #" + node_number +
                    "\" > iobench_run.log && " + nodes[i + 2] + " -s " +
                    std::to_string( file_size ) + " -c " +
                    std::to_string( file_count ) + " --creation-count " +
                    std::to_string( creation_count ) + " -t " +
                    std::to_string( threads ) + " --ssh;" +
                    " cat iobench_run.log >> " + cur_path +
                    "/iobench_run.log" + final_command );
            } else {
                std::cerr << "Failed to authenticate on server " << nodes[i]
                          << std::endl;
                return false;
            }
        } else {
            std::cerr << "Failed to start ssh" << std::endl;
            return false;
        }
    }

    return true;
}

// if there is a need to generate files, return false
void ensureFileCreation( std::string &benchmarks, bool &deleted ) {
    deleted = true; // at the beginning, files don't exist

    for ( size_t i = 0; i < benchmarks.size(); i++ ) {
        if ( benchmarks[i] == 'c' ) {
            // we don't care about creation
            continue;
        } else if ( deleted && benchmarks[i] == 'w' ) {
            // if deleted and benchmark is 'write' no need to create
            // files, they will be created by write
            deleted = false;
            // create 0 byte files, so metadata creation isn't counted
            // in the benchmark
            benchmarks.insert( i, "0" );
        } else if ( !deleted && benchmarks[i] == 'd' ) {
            // if deleting files, deleted is set to true
            deleted = true;
        } else if ( deleted ) {
            // if files don't exist and benchmark isn't 'write'
            // add 'create files' "benchmark"
            benchmarks.insert( i, "f" );
            deleted = false;
            i++;
        }
    }
}

void printHelp() {
    std::cout << "Usage:" << std::endl;
    std::cout << "  iobench [options]" << std::endl << std::endl;
    std::cout << "  -h, --help\tPrint this help" << std::endl;
    std::cout << std::endl << "FILE OPTIONS" << std::endl;
    std::cout << "  -c, --file-count\tNumber of files" << std::endl;
    std::cout << "  -s, --file-size\tSize of files" << std::endl;
    std::cout << "  --creation-count\tNumber of files for creation "
                 "benchmark (default is 100000)"
              << std::endl;
    std::cout << "  --path\t\tPath where to run benchmark" << std::endl;
    std::cout << "  --keep-files\t\tDon't delete created files (doesn't apply "
                 "to creation benchmark)"
              << std::endl;
    std::cout << std::endl << "BENCHMARK OPTIONS" << std::endl;
    std::cout << "  -t, --threads\t\tNumber of threads to use" << std::endl;
    std::cout << "  -n, --nodes\t\tName of configuration file for nodes to be "
                 "used in benchmark"
              << std::endl;
    std::cout << "  --config\t\tBenchmark configuration file" << std::endl;
    std::cout << "  --block-size\t\tSize of read/write block" << std::endl;
    std::cout << "  --write\t\tRun 'write' benchmark" << std::endl;
    std::cout << "  --read\t\tRun 'read' benchmark" << std::endl;
    std::cout << "  --move\t\tRun 'move' benchmark" << std::endl;
    std::cout << "  --remove\t\tRun 'remove' benchmark" << std::endl;
    std::cout << "  --delete\t\tRun 'remove' benchmark" << std::endl;
    std::cout << "  --creation\t\tRun 'creation' benchmark" << std::endl;
}

int parseCommandLine( int argc, char **argv, uint64_t &file_size,
                      uint64_t &file_count, uint64_t &creation_file_count,
                      int &threads, std::string &benchmarks, bool &ssh,
                      bool &delete_files, std::vector< std::string > &nodes,
                      std::string &path ) {
    static struct option long_options[] = {
        { "help",           no_argument,        0,  'h' },
        { "file-count",     required_argument,  0,  'c' },
        { "file-size",      required_argument,  0,  's' },
        { "creation-count", required_argument,  0,  '0' },
        { "threads",        required_argument,  0,  't' },
        { "nodes",          required_argument,  0,  'n' },
        { "move",           no_argument,        0,  'M' },
        { "read",           no_argument,        0,  'R' },
        { "remove",         no_argument,        0,  'D' },
        { "delete",         no_argument,        0,  'D' },
        { "creation",       no_argument,        0,  'C' },
        { "write",          no_argument,        0,  'W' },
        { "config",         required_argument,  0,  '1' },
        { "block-size",     required_argument,  0,  '2' },
        { "path",           required_argument,  0,  '3' },
        { "ssh",            no_argument,        0,  '4' },
        { "keep-files",     no_argument,        0,  '5' },
        { 0,                0,                  0,  0   }
        // need zeroed element to correctly report wrong arguments
    };

    char *tmp;             // needed for strtoull
    uint64_t block_size{}; // needed for block-size argument

    while ( 1 ) {
        int option_index{ 0 };
        auto c =
            getopt_long( argc, argv, "hc:s:t:n:", long_options, &option_index );
        if ( c == -1 )
            break;
        switch ( c ) {
        case 'h': // help
            printHelp();
            return PARSE_HELP;
        case 'c': // file-count
            file_count = std::strtoull( optarg, &tmp, 10 );
            humanToNumber( lastChar( optarg ), file_count );
            break;
        case 's': // file-size
            file_size = std::strtoull( optarg, &tmp, 10 );
            humanToNumber( lastChar( optarg ), file_size );
            break;
        case '0': // creation-count
            creation_file_count = std::strtoull( optarg, &tmp, 10 );
            humanToNumber( lastChar( optarg ), creation_file_count );
            break;
        case 't': // threads
            threads = std::atoi( optarg );
            break;
        case 'n': // nodes
            nodes = parseNodesFile( optarg );
            if ( nodes.empty() ) {
                std::cerr
                    << "Node configuration file " << optarg
                    << " either doesn't specify any nodes or cannot be parsed"
                    << std::endl;
                return PARSE_ERROR;
            }
            break;
        case 'M': // move
            benchmarks += 'm';
            break;
        case 'R': // read
            benchmarks += 'r';
            break;
        case 'D': // remove/delete
            benchmarks += 'd';
            break;
        case 'C': // creation
            benchmarks += 'c';
            break;
        case 'W': // write
            benchmarks += 'w';
            break;
        case '1': // config
            if ( !parseConfig( optarg, file_count, file_size,
                               creation_file_count, threads, delete_files,
                               nodes, benchmarks, path ) )
                return PARSE_ERROR;
            break;
        case '2': // block-size
            block_size = std::strtoull( optarg, &tmp, 10 );
            humanToNumber( lastChar( optarg ), block_size );
            setBlockSize( block_size );
            break;
        case '3': // path
            path = FSLib::canonical( optarg );
            if ( !FSLib::exists( path ) ) {
                std::cerr << "Provided path doesn't exist " << path
                          << std::endl;
                return PARSE_ERROR;
            }
            break;
        case '4': // ssh
            ssh = true;
            break;
        case '5': // keep-files
            delete_files = false;
            break;
        default:
            return PARSE_ERROR;
        }
    }

    return PARSE_OK;
}

bool nodeBenchmark( std::string &benchmarks,
                    const std::vector< std::string > &nodes,
                    uint64_t &file_count, uint64_t &file_size,
                    uint64_t &creation_file_count, int &threads,
                    const bool &delete_files, const std::string &path ) {
    // benchmark on network connected nodes
    // have to keep SSH objects to keep connection alive
    std::vector< std::unique_ptr< SSH > > ssh_connections{};

    bool deleted{ true };

    ensureFileCreation( benchmarks, deleted );

    if ( !startNodes( nodes, ssh_connections, file_count, file_size,
                      creation_file_count, threads, delete_files, path ) )
        return false;

    // handle reported results and send info on which benchmarks are to be run
    start_server( benchmarks, ssh_connections );
    // nicely wait for the processes to finish
    for ( const auto &x : ssh_connections ) {
        x->commandOutputBlockOnce();
    }

    return true;
}

void localBenchmark( std::string &benchmarks, uint64_t &file_count,
                     uint64_t &file_size, uint64_t &creation_file_count,
                     int &threads, const bool &delete_files,
                     const std::string &path ) {
    std::vector< std::vector< std::string > > files{};

    bool deleted{ true };

    ensureFileCreation( benchmarks, deleted );

    for ( unsigned long i = 0; i < benchmarks.size(); i++ ) {
        auto results =
            runBenchmark( benchmarks[i], files, file_size, file_count,
                          creation_file_count, threads, path );
        if ( results.empty() ) {
            std::cerr << "Something went wrong, check iobench_run.log"
                      << std::endl;
            if ( delete_files ) {
                multiCleanUp( files, path );
                if ( FSLib::exists( path + "/creation" ) )
                    multiCleanUpCreation( creation_file_count, threads,
                                          path + "/creation" );
            }
            return;
        }

        for ( const auto &x : results ) {
            if ( x == LDBL_MAX ) {
                std::cerr << "Something went wrong, check iobench_run.log"
                          << std::endl;
                if ( delete_files ) {
                    multiCleanUp( files, path );
                    if ( FSLib::exists( path + "/creation" ) )
                        multiCleanUpCreation( creation_file_count, threads,
                                              path + "/creation" );
                }
                return;
            }
        }

        printResults( benchmarks[i], results );
    }

    std::cout << "\x1b[1A" << std::flush; // move up one line (empty line caused
                                          // by printResults)

    if ( !deleted && delete_files ) {
        multiCleanUp( files, path );
        if ( FSLib::exists( path + "/creation" ) )
            multiCleanUpCreation( creation_file_count, threads,
                                  path + "/creation" );
    } else {
        multiRemoveDirs( files.size(), path );
        if ( FSLib::exists( path + "/creation" ) )
            multiCleanUpCreation( creation_file_count, threads,
                                  path + "/creation" );
    }
}

void sshBenchmark( uint64_t &file_count, uint64_t &file_size,
                   uint64_t &creation_file_count, int &threads,
                   const bool &delete_files, const std::string &path ) {
    // benchmark that sends results to server
    std::vector< std::vector< std::string > > files;
    char nextBench = 'k';
    std::cout << "k" << std::endl; // report online

    LOG_time( "Reported online" );

    std::cin >> nextBench;
    std::cin.clear();
    std::cin.ignore( 1000, '\n' );

    LOG_time( "Got nextbench" );

    // help from
    // https://stackoverflow.com/questions/554063/how-do-i-print-a-double-value-with-full-precision-using-cout
    // need to set precision to report whole numbers instead of 1.33742e+08
    std::cout.precision( std::numeric_limits< long double >::max_digits10 );

    // when server sends 'k' benchmark it means "kill"
    while ( nextBench != 'k' ) {
        auto results = runBenchmark( nextBench, files, file_size, file_count,
                                     creation_file_count, threads, path );

        if ( results.empty() ) {
            std::cerr << "Something went wrong, check iobench_run.log"
                      << std::endl;
            if ( delete_files ) {
                multiCleanUp( files, path );
                if ( FSLib::exists( path + "/creation" ) )
                    multiCleanUpCreation( creation_file_count, threads,
                                          path + "/creation" );
            }
            return;
        }

        std::string report;

        for ( const auto &x : results ) {
            if ( x == LDBL_MAX ) {
                std::cerr << "Something went wrong, check iobench_run.log"
                          << std::endl;
                if ( delete_files ) {
                    multiCleanUp( files, path );
                    if ( FSLib::exists( path + "/creation" ) )
                        multiCleanUpCreation( creation_file_count, threads,
                                              path + "/creation" );
                }
                return;
            }

            report += std::to_string( x ) + ",";
        }

        LOG_time( "Benchmark: " + std::string( 1, nextBench ) );
        if ( !report.empty() ) {
            // LOG
            LOG_time( "Reported result " + report );

            std::cout << report << std::endl;
        } else {
            std::cout << std::endl;
        }

        std::cin >> nextBench;
        std::cin.clear();
        std::cin.ignore( 1000, '\n' );
    }
}

void sanitizeInput( const std::string &buffer, uint64_t &pos,
                    std::string &target ) {
    char end_char{ ' ' };
    // skp leading white space
    while ( std::isspace( buffer[pos] ) && pos < buffer.size() )
        pos++;

    // if first char is " or ' use it as split indicator
    if ( buffer[pos] == '"' ) {
        end_char = '"';
        pos++;
    } else if ( buffer[pos] == '\'' ) {
        end_char = '\'';
        pos++;
    }

    if ( pos >= buffer.size() )
        return;

    bool ignore_next{ false }; // if true, next char loses all special
                               // properties (e.g. end_char is not splitting)

    while ( ( buffer[pos] != end_char || ignore_next ) &&
            pos < buffer.size() ) {
        if ( std::isspace( buffer[pos] ) && !ignore_next ) {
            target += "\\ ";
        } else if ( buffer[pos] == ';' && !ignore_next ) {
            target += "\\;";
        } else if ( buffer[pos] == '`' && !ignore_next ) {
            target += "\\`";
        } else if ( buffer[pos] == '\\' && !ignore_next ) {
            target += '\\';
            ignore_next = true;
            pos++;
            continue;
        } else {
            target += buffer[pos];
        }
        pos++;
        ignore_next = false;
    }
    pos++; // we want pos to be just after the end_char
}

// if line in file is a comment, return true
bool checkComment( const std::string &buffer, uint64_t &pos ) {
    while ( std::isspace( buffer[pos] ) )
        pos++;
    if ( buffer[pos] == '#' || buffer[pos] == ';' )
        return true;
    return false;
}

bool parseNodeLine( const std::string &line, std::vector< std::string > &nodes,
                    const uint64_t &initial_pos, const std::string &user ) {
    if ( line.empty() )
        return true; // empty line is valid

    uint64_t pos{ initial_pos };
    if ( checkComment( line, pos ) )
        return true; // comment line is valid

    std::string first{}, second{}, third{}, port{};

    sanitizeInput( line, pos, first );
    sanitizeInput( line, pos, second );
    sanitizeInput( line, pos, third );

    // check if port is specified
    auto colon_pos = first.find_first_of( ':' );
    if ( colon_pos != std::string::npos ) {
        port = first.substr( colon_pos + 1 );
        first = first.substr( 0, colon_pos );
        for ( auto &x : port ) {
            if ( !std::isdigit( x ) ) {
                std::cerr << "Line: \'" << line
                          << "\' of nodes configuration is not valid (port is "
                             "not a number)"
                          << std::endl;
                return false; // port must be numbers only
            }
        }
    }

    if ( second.empty() ) { // this means that at least one of the required
                            // fields is not present
        std::cerr
            << "Line: \'" << line
            << "\' of nodes configuration is not valid (required field missing)"
            << std::endl;
        return false;
    }

    nodes.push_back( first );
    // if user wasn't specified, use user that started the software
    nodes.push_back( third.empty() ? user : second );
    nodes.push_back( third.empty() ? second : third );
    nodes.push_back( port );

    return true;
}

std::vector< std::string > parseNodesFile( const std::string &file_path,
                                           const std::string &user ) {
    std::ifstream file( file_path );
    std::string buffer;
    std::vector< std::string > nodes;

    while ( std::getline( file, buffer ) ) {
        if ( !parseNodeLine( buffer, nodes, 0, user ) )
            return {};
    }

    return nodes;
}

bool parseConfig( const std::string &file_path, uint64_t &file_count,
                  uint64_t &file_size, uint64_t &creation_file_count,
                  int &threads, bool &delete_files,
                  std::vector< std::string > &nodes, std::string &benchmarks,
                  std::string &path ) {
    std::ifstream file( file_path );
    std::string buffer;
    int line{ 0 };
    bool nodes_config{ false };
    std::string user{ getCurrentPWD()->pw_name };

    while ( std::getline( file, buffer ) ) {
        line++;

        if ( buffer.empty() )
            continue;

        uint64_t pos{};

        if ( checkComment( buffer, pos ) )
            continue;

        std::string word;
        std::stringstream ss( buffer );
        ss >> word;

        if ( word == "files" ) {
            ss >> word;
            file_count = std::stoull( word );
            humanToNumber( word[word.size() - 1], file_count );
        } else if ( word == "size" ) {
            ss >> word;
            file_size = std::stoull( word );
            humanToNumber( word[word.size() - 1], file_size );
        } else if ( word == "creation-files" ) {
            ss >> word;
            creation_file_count = std::stoull( word );
            humanToNumber( word[word.size() - 1], creation_file_count );
        } else if ( word == "block-size" ) {
            ss >> word;
            uint64_t bs = std::stoull( word );
            humanToNumber( word[word.size() - 1], bs );
            setBlockSize( bs );
        } else if ( word == "threads" ) {
            ss >> threads;
        } else if ( word == "benchmarks" ) {
            while ( ss >> word ) {
                if ( word == "write" )
                    benchmarks += 'w';
                else if ( word == "read" )
                    benchmarks += 'r';
                else if ( word == "move" )
                    benchmarks += 'm';
                else if ( word == "delete" || word == "remove" )
                    benchmarks += 'd';
                else if ( word == "creation" )
                    benchmarks += 'c';
            }
        } else if ( word == "nodes" ) {
            if ( !nodes.empty() )
                std::cerr << "On line " << line
                          << " you are loading nodes from configuration file "
                             "even though some nodes were already set up. "
                             "Please take note of this. Nodes that were "
                             "specified previously will be ignored."
                          << std::endl;
            nodes_config = true;
            ss >> word;
            nodes = parseNodesFile( word, user );
            if ( nodes.empty() ) {
                std::cerr
                    << "Node configuration file " << word
                    << " either doesn't specify any nodes or cannot be parsed"
                    << std::endl;
                return false;
            }
        } else if ( word == "node" ) {
            if ( nodes_config )
                std::cerr
                    << "On line " << line
                    << " you are adding a node even though you've loaded nodes "
                       "from a configuration file. Please take note of this."
                    << std::endl;
            parseNodeLine( ss.str(), nodes, ss.tellg(), user );
        } else if ( word == "path" ) {
            ss >> path;
            if ( !FSLib::exists( path ) ) {
                std::cerr << "Provided path doesn't exist " << path
                          << std::endl;
                return false;
            }
        } else if ( word == "keep-files" ) {
            delete_files = false;
        } else {
            std::cerr << "Error on line " << line << " of configuration file, '"
                      << word << "' is not a valid configuration word"
                      << std::endl;
            return false;
        }
    }

    return true;
}

void resetLOG() {
    std::ofstream log( "iobench_run.log" );
    log << "IOBENCH RUN LOG" << std::endl;
}
