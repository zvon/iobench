CXX ?= clang++
CFLAGS ?= -O2 -Wall -Wextra -std=c++11
PREFIX ?= /usr/local/bin

.PHONY: default
default: iobench

iobench: main.cpp functions.o network.o ssh.o benchmark.o fslib.o utilities.o
	$(CXX) $(CFLAGS) -o iobench main.cpp functions.o network.o ssh.o benchmark.o fslib.o utilities.o -lpthread -lssh

functions.o: functions.cpp
	$(CXX) $(CFLAGS) -c functions.cpp

network.o: Network/network.cpp
	$(CXX) $(CFLAGS) -c Network/network.cpp

ssh.o: Network/ssh.cpp
	$(CXX) $(CFLAGS) -c Network/ssh.cpp

benchmark.o: FSLib/benchmark.cpp
	$(CXX) $(CFLAGS) -c FSLib/benchmark.cpp

fslib.o: FSLib/fslib.cpp
	$(CXX) $(CFLAGS) -c FSLib/fslib.cpp

utilities.o: FSLib/utilities.cpp
	$(CXX) $(CFLAGS) -c FSLib/utilities.cpp

.PHONY: debug
debug: iobench_debug.out

iobench_debug.out: main.cpp functions.o network.o ssh.o benchmark.o fslib.o utilities.o
	$(CXX) $(CFLAGS) -g -DDEBUG -o iobench_debug.out main.cpp functions.o network.o ssh.o benchmark.o fslib.o utilities.o -lpthread -lssh

.PHONY: clean
clean:
	rm -Rf *.out *.o iobench

.PHONY: install
install: iobench
	install -d $(PREFIX)/
	install -m 755 iobench $(PREFIX)/
