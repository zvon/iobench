#ifndef FSLIB_H
#define FSLIB_H

#include <string>
#include <vector>

namespace FSLib {
int copyFile( const std::string &file_a, const std::string &file_b );

// read file nad measure how long it takes
// has unused parameter so generic can work
uint64_t benchRead( const std::vector< std::string > &files );

// write file nad measure how long it takes
uint64_t benchWrite( const std::vector< std::string > &files,
                     const uint64_t &file_size );

// create a file with random content
void randomFile( const std::string &file_, const uint64_t &size_ );

// delete files in `files` located in `path` that have `prefix`
void cleanUp( const std::vector< std::string > &files,
              const std::string &path = ".", const std::string &prefix = "" );

void cleanUpCreation( const uint64_t &file_count, const std::string &path );

// move file `file_` to `target_`
int move( const std::string &file_, const std::string &target_ );

// remove file `file_`
int remove( const std::string &file_ );

// equivalent to `mkdir -p path`
bool createDirectoryFull( const std::string &path );

// check if file `path` exists
bool exists( const std::string &path );
uint64_t filesize( const std::string &path );

// convert path to canonical representation in the operating system
std::string canonical( const char *const path );
std::string canonical( const std::string &path );
} // end of namespace FSLib

// randomly swap elements of `vec`
void scrambleVector( std::vector< std::string > &vec );
void setBlockSize( const uint64_t &bs );

#endif
