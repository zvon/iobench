#ifndef FSUTIL_H
#define FSUTIL_H

#include <pwd.h>
#include <string>
#include <vector>

// convert num to human readable format
// return char corresponding to that format
char humanReadable( long double &num );

// convert num to computer readable format
// based on given unit
void humanToNumber( char unit, uint64_t &num );

// write log_string to log file
void LOG( const std::string &log_string );

// print result of provided benchmark
void printResults( char benchmark, const long double &complete,
                   const long double &min, char size_complete, char size_min,
                   const long double &min_node = 0, char size_min_node = '_' );

void printResults( char benchmark, std::vector< long double > &results );
void printResults( char benchmark,
                   std::vector< std::vector< long double > > &results );

// get /etc/password entry for current user
struct passwd *getCurrentPWD();
// get current working directory
std::string cwd();
// get hostname
std::string hostname();
// split string by given char
std::vector< std::string > splitString( const std::string &str, const char c );

#endif
