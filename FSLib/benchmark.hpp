#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <string>
#include <vector>

long double writeBenchmark( std::vector< std::string > &files,
                            uint64_t file_size, uint64_t file_count,
                            const std::string &path = "." );

long double readBenchmark( std::vector< std::string > &files,
                           uint64_t file_size, uint64_t file_count,
                           const std::string &path = "." );

long double moveBenchmark( std::vector< std::string > &files,
                           uint64_t file_size, uint64_t file_count,
                           const std::string &path = "." );

long double removeBenchmark( std::vector< std::string > &files,
                             uint64_t file_size, uint64_t file_count,
                             const std::string &path = "." );

long double creationBenchmark( std::vector< std::string > &files,
                               uint64_t file_size, uint64_t file_count,
                               const std::string &path = "." );

bool cleanUpMoved( const std::vector< std::string > &files,
                   const std::string &path = "." );

// 2 unused parameters so the function can be passed as
// an argument for functions that use cleanUpMoved
bool noCleanUp( const std::vector< std::string > & /*UNUSED*/,
                const std::string & /*UNUSED*/ );

#endif
