#include "utilities.hpp"
#include <fstream>
#include <iostream>
#include <limits.h>
#include <memory>
#include <numeric>
#include <unistd.h>

char humanReadable( long double &num ) {
    char size = '\0';
    if ( num > 1125899906842624 ) {
        num /= 1125899906842624;
        size = 'P';
    } else if ( num > 1099511627776 ) {
        num /= 1099511627776;
        size = 'T';
    } else if ( num > 1073741824 ) {
        num /= 1073741824;
        size = 'G';
    } else if ( num > 1048576 ) {
        num /= 1048576;
        size = 'M';
    } else if ( num > 1024 ) {
        num /= 1024;
        size = 'K';
    }

    return size;
}

void humanToNumber( char unit, uint64_t &num ) {
    switch ( unit ) {
    case 'P':
    case 'p':
        num *= 1125899906842624;
        break;
    case 'T':
    case 't':
        num *= 1099511627776;
        break;
    case 'G':
    case 'g':
        num *= 1073741824;
        break;
    case 'M':
    case 'm':
        num *= 1048576;
        break;
    case 'K':
    case 'k':
        num *= 1024;
    default:
        break;
    }
}

void LOG( const std::string &log_string ) {
    std::fstream log( "iobench_run.log" );
    log.seekp( 0, std::ios_base::end );
    log << log_string << std::endl;
}

// print results for node benchmark
void printResults( char benchmark,
                   std::vector< std::vector< long double > > &results ) {
    long double min_node{}, min_thread{}, complete{};

    // get minimal result from all values
    min_thread = std::accumulate(
        results.begin(), results.end(), results[0][0],
        []( long double &a, std::vector< long double > &b ) {
            auto min_vec = std::accumulate(
                b.begin(), b.end(), b[0], []( long double &c, long double &d ) {
                    if ( c > d ) {
                        return d;
                    }
                    return c;
                } );
            if ( a > min_vec ) {
                return min_vec;
            }
            return a;
        } );

    // generate a vector of "complete" values (all values from each vector added
    // together)
    std::vector< long double > complete_vec;
    for ( auto &x : results ) {
        complete_vec.push_back( std::accumulate(
            x.begin(), x.end(), 0.0,
            []( long double a, long double &b ) { return a + b; } ) );
    }

    // get minimal result form "complete" values
    min_node =
        std::accumulate( complete_vec.begin(), complete_vec.end(),
                         complete_vec[0], []( long double &a, long double &b ) {
                             if ( a > b ) {
                                 return b;
                             }
                             return a;
                         } );

    // add together all results
    complete = std::accumulate(
        complete_vec.begin(), complete_vec.end(), 0.0,
        []( long double a, long double &b ) { return a + b; } );

    printResults( benchmark, complete, min_thread, humanReadable( complete ),
                  humanReadable( min_thread ), min_node,
                  humanReadable( min_node ) );
}

// print results for local benchmark
void printResults( char benchmark, std::vector< long double > &results ) {
    long double min{}, complete{};

    // get minimal result
    min = std::accumulate( results.begin(), results.end(), results[0],
                           []( long double &a, long double &b ) {
                               if ( a > b ) {
                                   return b;
                               }
                               return a;
                           } );

    // add all results together
    complete = std::accumulate(
        results.begin(), results.end(), 0.0,
        []( long double a, long double &b ) { return a + b; } );

    printResults( benchmark, complete, min, humanReadable( complete ),
                  humanReadable( min ) );
}

// print results
void printResults( char benchmark, const long double &complete,
                   const long double &min, char size_complete, char size_min,
                   const long double &min_node, char size_min_node ) {
    switch ( benchmark ) {
    case 'c':
        std::cout << "Average creation:" << std::endl;
        std::cout << "Total: " << complete << size_complete << " files/s"
                  << std::endl;

        // if size_min_node == '_' it means this is a local benchmark
        if ( size_min_node == '_' ) {
            std::cout << "Min per process: ";
        } else {
            std::cout << "Min per node: ";
            std::cout << min_node << size_min_node << " files/s" << std::endl;
            std::cout << "Min per process: ";
        }

        std::cout << min << size_min << " files/s" << std::endl;
        break;
    case 'm':
        std::cout << "Average move:" << std::endl;
        std::cout << "Total: " << complete << size_complete << " files/s"
                  << std::endl;

        if ( size_min_node == '_' ) {
            std::cout << "Min per process: ";
        } else {
            std::cout << "Min per node: ";
            std::cout << min_node << size_min_node << " files/s" << std::endl;
            std::cout << "Min per process: ";
        }

        std::cout << min << size_min << " files/s" << std::endl;
        break;
    case 'r':
        std::cout << "Average read:" << std::endl;
        std::cout << "Total: " << complete << " " << size_complete << "B/s"
                  << std::endl;

        if ( size_min_node == '_' ) {
            std::cout << "Min per process: ";
        } else {
            std::cout << "Min per node: ";
            std::cout << min_node << " " << size_min_node << "B/s" << std::endl;
            std::cout << "Min per process: ";
        }

        std::cout << min << " " << size_min << "B/s" << std::endl;
        break;
    case 'd':
        std::cout << "Average remove:" << std::endl;
        std::cout << "Total: " << complete << size_complete << " files/s"
                  << std::endl;

        if ( size_min_node == '_' ) {
            std::cout << "Min per process: ";
        } else {
            std::cout << "Min per node: ";
            std::cout << min_node << size_min_node << " files/s" << std::endl;
            std::cout << "Min per process: ";
        }

        std::cout << min << size_min << " files/s" << std::endl;
        break;
    case 'w':
        std::cout << "Average write:" << std::endl;
        std::cout << "Total: " << complete << " " << size_complete << "B/s"
                  << std::endl;

        if ( size_min_node == '_' ) {
            std::cout << "Min per process: ";
        } else {
            std::cout << "Min per node: ";
            std::cout << min_node << " " << size_min_node << "B/s" << std::endl;
            std::cout << "Min per process: ";
        }

        std::cout << min << " " << size_min << "B/s" << std::endl;
        break;
    case 'f':
    case '0':
        std::cout << "\x1b[1A";
    default:
        break;
    }
    std::cout << std::endl;
}

struct passwd *getCurrentPWD() {
    uid_t user_uid;
    {
        uid_t eid;
        uid_t sid;
        getresuid( &user_uid, &eid, &sid ); // don't need eid and sid
    }
    auto user_passwd = getpwuid( user_uid );

    if ( user_passwd == nullptr )
        throw std::runtime_error(
            "User with uid " + std::to_string( user_uid ) + " doesn't exist!" );
    return user_passwd;
}

std::string cwd() {
    std::unique_ptr< char[] > cwd_char( new char[PATH_MAX] );

    if ( getcwd( cwd_char.get(), PATH_MAX ) == nullptr ) {
        return "."; // this is always true even though it might not be desired
    }

    return cwd_char.get();
}

std::string hostname() {
    std::unique_ptr< char[] > host( new char[HOST_NAME_MAX] );

    if ( gethostname( host.get(), HOST_NAME_MAX ) == -1 ) {
        return "localhost"; // this is always true even though it might not be
                            // desired
    }

    return host.get();
}

std::vector< std::string > splitString( const std::string &str, const char c ) {
    unsigned long pos{};
    std::vector< std::string > split;
    do {
        auto old_pos = pos;
        pos = str.find_first_of( c, pos );
        auto push = str.substr( old_pos, pos - old_pos );
        if ( !push.empty() && push != "\n" )
            split.push_back( std::move( push ) );
        pos++;
    } while ( pos != 0 );
    return split;
}
