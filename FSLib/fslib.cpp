#include "fslib.hpp"
#include "utilities.hpp"
#include <chrono>
#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <fstream>
#include <limits.h>
#include <memory>
#include <random>
#include <sys/stat.h>
#include <unistd.h>

const std::string allowedChars =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

std::mt19937 gen;
std::uniform_int_distribution<> dist( 0, allowedChars.length() - 1 );

bool initiated = false;
uint64_t block_size = 524288; // 512 KiB

int FSLib::copyFile( const std::string &file_a, const std::string &file_b ) {
    // copied from
    // https://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c
    std::unique_ptr< char[] > buf( new char[block_size] );
    ssize_t nread;
    int saved_errno;

    // origin file
    auto fd_from = open( file_a.c_str(), O_RDONLY );
    if ( fd_from < 0 )
        return -1;

    // destination file
    auto fd_to = open( file_b.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644 );
    if ( fd_to < 0 ) {
        saved_errno = errno;
        close( fd_from );
        errno = saved_errno;
        return -1;
    }

    while ( nread = read( fd_from, buf.get(), block_size ), nread > 0 ) {
        char *out_ptr = buf.get();
        ssize_t nwritten{};
        do {
            // write same amount of bytes that was read
            nwritten = write( fd_to, out_ptr, nread );
            if ( nwritten >= 0 ) {
                // move forward by nwritten bytes
                nread -= nwritten;
                out_ptr += nwritten;
            } else if ( errno != EINTR ) {
                saved_errno = errno;
                close( fd_from );
                close( fd_to );
                errno = saved_errno;
                return -1;
            }
        } while ( nread > 0 );
    }

    if ( nread == 0 ) {
        if ( close( fd_to ) < 0 ) {
            saved_errno = errno;
            close( fd_from );
            errno = saved_errno;
            return -1;
        }
        close( fd_from );
        return 0;
    }

    saved_errno = errno;
    close( fd_to );
    close( fd_from );
    errno = saved_errno;
    return -1;
}

uint64_t FSLib::benchRead( const std::vector< std::string > &files ) {
    // based on
    // https://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c
    std::unique_ptr< char[] > buf( new char[block_size] );
    auto *buf_ptr = buf.get();
    ssize_t nread;
    int saved_errno;

    // start measuring BEFORE opening file
    auto start = std::chrono::steady_clock::now();

    for ( const auto &file : files ) {
        auto fd_from = open( file.c_str(), O_RDONLY );

        if ( fd_from < 0 ) {
            LOG( "Couldn't open file " + file );
            LOG( strerror( errno ) );
            return ULLONG_MAX;
        }

        while ( nread = read( fd_from, buf_ptr, block_size ), nread > 0 ) {}

        if ( nread != 0 ) {
            LOG( "Couldn't read the last part of the file " + file );
            LOG( strerror( errno ) );
            saved_errno = errno;
            close( fd_from );
            errno = saved_errno;
            return ULLONG_MAX;
        }

        close( fd_from );
    }

    auto end = std::chrono::steady_clock::now();

    // return difference between start and end in nanoseconds
    return std::chrono::duration_cast< std::chrono::nanoseconds >( end - start )
        .count();
}

uint64_t FSLib::benchWrite( const std::vector< std::string > &files,
                            const uint64_t &file_size ) {
    std::unique_ptr< char[] > buf( new char[block_size] );
    // if random generator hasn't been initiated, initiate it
    if ( !initiated ) {
        gen.seed( std::random_device()() );
        initiated = true;
    }

    auto base = dist( gen );

    // generate random string to be written to the file
    for ( uint64_t i = 0; i < block_size; i++ ) {
        buf[i] = allowedChars[base];
        base = dist( gen );
        if ( static_cast< uint64_t >( base ) >= allowedChars.size() )
            base -= allowedChars.size();
    }

    // based on
    // https://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c
    int saved_errno;
    ssize_t nwritten{};
    uint64_t block = 0;

    // number of bytest left after writing last set block_size
    auto last_few = file_size % block_size;

    auto *buf_ptr = buf.get();

    auto start = std::chrono::steady_clock::now();

    for ( const auto &file : files ) {
        // open file for write only with possibility to create it if it doesn't
        // exist already
        auto fd_to = open( file.c_str(), O_WRONLY, 0644 );

        if ( fd_to < 0 ) {
            LOG( "Couldn't open file " + file );
            LOG( strerror( errno ) );
            LOG( cwd() );
            return ULLONG_MAX;
        }

        for ( block = 0; block < file_size / block_size; block++ ) {
            nwritten = write( fd_to, buf_ptr, block_size );
            if ( nwritten < 0 && errno != EINTR ) {
                LOG( "Couldn't write to file " + file );
                LOG( strerror( errno ) );
                saved_errno = errno;
                close( fd_to );
                errno = saved_errno;
                return ULLONG_MAX;
            }
        }

        if ( last_few )
            nwritten = write( fd_to, buf.get(), last_few );

        if ( nwritten < 0 ) {
            LOG( "Couldn't write the last part to the file " + file );
            LOG( strerror( errno ) );
            saved_errno = errno;
            close( fd_to );
            errno = saved_errno;
            return ULLONG_MAX;
        }

        if ( close( fd_to ) < 0 ) {
            LOG( "Couldn't close file " + file );
            LOG( strerror( errno ) );
            return ULLONG_MAX;
        }
    }

    auto end = std::chrono::steady_clock::now();

    // return difference between start and end in nanoseconds
    return std::chrono::duration_cast< std::chrono::nanoseconds >( end - start )
        .count();
}

void FSLib::randomFile( const std::string &file_, const uint64_t &size_ ) {
    std::unique_ptr< char[] > buf( new char[block_size] );

    // if random generator hasn't been initiated, initiate it
    if ( !initiated ) {
        gen.seed( std::random_device()() );
        initiated = true;
    }

    auto base = dist( gen );

    // generate random string to be written to the file
    for ( uint64_t i = 0; i < block_size; i++ ) {
        buf[i] = allowedChars[base];
        base = dist( gen );
        if ( static_cast< uint64_t >( base ) >= allowedChars.size() )
            base -= allowedChars.size();
    }

    std::ofstream file( file_ );
    for ( uint64_t i = 0; i < size_ / block_size; i++ ) {
        file.write( buf.get(), block_size );
    }
    file.write( buf.get(), size_ % block_size );
}

void FSLib::cleanUp( const std::vector< std::string > &files,
                     const std::string &path, const std::string &prefix ) {
    for ( const auto &x : files ) {
        FSLib::remove( path + "/" + prefix + x );
    }
}

void FSLib::cleanUpCreation( const uint64_t &file_count,
                             const std::string &path ) {
    for ( uint64_t i = 0; i < file_count; i++ ) {
        FSLib::remove( path + "/file_" + std::to_string( i ) );
    }
}

int FSLib::move( const std::string &file_, const std::string &target_ ) {
    return rename( file_.c_str(), target_.c_str() );
}

int FSLib::remove( const std::string &file_ ) {
    return ::remove( file_.c_str() );
}

void scrambleVector( std::vector< std::string > &vec ) {
    std::uniform_int_distribution<> vecDist( 0, vec.size() - 1 );
    // initiate random generator if it hasn't been initiated already
    if ( !initiated ) {
        gen.seed( std::random_device()() );
        initiated = true;
    }
    // randomly swap elements of vector
    for ( uint64_t i = 0; i < vec.size() * 2; i++ ) {
        auto first = vecDist( gen );
        auto second = vecDist( gen );
        vec[first].swap( vec[second] );
    }
}

bool FSLib::createDirectoryFull( const std::string &path ) {
    uint64_t pos{};
    // go through all directories leading to the last one
    // and create them if they don't exist
    do {
        // get partial directory path
        pos = path.find_first_of( "/", pos );
        if ( pos > 0 ) {
            auto dirname = path.substr( 0, pos );
            // create it if it doesn't exist
            if ( !FSLib::exists( dirname ) ) {
                if ( mkdir( dirname.c_str(),
                            S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ) != 0 )
                    return false;
            }
        }
        pos++;
    } while ( pos < path.length() && pos != 0 );
    return true;
}

bool FSLib::exists( const std::string &path ) {
    struct stat path_stat;
    // if file exists, stat returns 0
    return stat( path.c_str(), &path_stat ) == 0;
}

uint64_t FSLib::filesize( const std::string &path ) {
    struct stat path_stat;
    // if file exists, stat returns 0
    if ( stat( path.c_str(), &path_stat ) == 0 ) {
        return path_stat.st_size;
    }
    return 0;
}

void setBlockSize( const uint64_t &bs ) {
    block_size = bs;
}

std::string FSLib::canonical( const char *const path ) {
    std::unique_ptr< char[] > canonical_path( new char[PATH_MAX] );
    std::unique_ptr< char[] > path_full;

    // if first char of path is ~ replace it with user's home directory
    if ( path[0] == '~' ) {
        char *homedir = getCurrentPWD()->pw_dir;
        // don't need +1 for '\0' because we won't be using first char of `path`
        path_full.reset(
            new char[std::strlen( homedir ) + std::strlen( path )] );
        std::strcpy( path_full.get(), homedir );
        std::strcat( path_full.get(),
                     path + 1 ); // skip '~' character when copying
    }

    // see which path should be translated
    const char *const real_path =
        ( path_full == nullptr ) ? path : path_full.get();

    // convert real_path to canonical representation and store it in
    // canonical_path
    if ( realpath( real_path, canonical_path.get() ) == nullptr ) {
        return path; // return original path, so software can continue
    }

    return canonical_path.get();
}

std::string FSLib::canonical( const std::string &path ) {
    return canonical( path.c_str() );
}
