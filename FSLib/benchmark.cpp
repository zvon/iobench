#include "benchmark.hpp"
#include "fslib.hpp"
#include "utilities.hpp"
#include <cfloat>
#include <chrono>
#include <cstring>
#include <fcntl.h>
#include <functional>
#include <iostream>
#include <limits.h>
#include <unistd.h>

long double writeBenchmark( std::vector< std::string > &files,
                            uint64_t file_size, uint64_t /*UNUSED*/,
                            const std::string &path ) {
    scrambleVector( files );
    std::vector< std::string > files_path;
    // to ensure no additional time measured by creating filename strings
    for ( auto &file : files ) {
        files_path.push_back( path + "/" + file );
    }

    long double average = FSLib::benchWrite( files_path, file_size );

    if( average == ULLONG_MAX )
        return LDBL_MAX;

    average /= files.size();
    // average in seconds
    average /= 1000000000;

    return file_size / average;
}

long double readBenchmark( std::vector< std::string > &files,
                           uint64_t file_size, uint64_t /*UNUSED*/,
                           const std::string &path ) {
    scrambleVector( files );
    std::vector< std::string > files_path;
    // to ensure no additional time measured by creating filename strings
    for ( auto &file : files ) {
        files_path.push_back( path + "/" + file );
    }

    long double average = FSLib::benchRead( files_path );

    if( average == ULLONG_MAX )
        return LDBL_MAX;

    average /= files.size();
    // average in seconds
    average /= 1000000000;

    return file_size / average;
}

long double moveBenchmark( std::vector< std::string > &files,
                           uint64_t /*UNUSED*/, uint64_t /*UNUSED*/,
                           const std::string &path ) {
    scrambleVector( files );
    std::vector< std::string > files_path;
    std::vector< std::string > files_target;
    // to ensure no additional time measured by creating filename strings
    for ( auto &file : files ) {
        files_path.push_back( path + "/" + file );
        files_target.push_back( path + "/moved_" + file );
    }

    auto result = 0;
    auto start = std::chrono::steady_clock::now();

    for ( unsigned long i = 0; i < files_path.size(); i++ ) {
        result = FSLib::move( files_path[i], files_target[i] );
        if ( result != 0 ) {
            return LDBL_MAX;
        }
    }

    auto end = std::chrono::steady_clock::now();

    long double average =
        std::chrono::duration_cast< std::chrono::nanoseconds >( end - start )
            .count();

    average /= files.size();
    // average in seconds
    average /= 1000000000;

    return 1.0 / average;
}

long double removeBenchmark( std::vector< std::string > &files,
                             uint64_t /*UNUSED*/, uint64_t /*UNUSED*/,
                             const std::string &path ) {
    scrambleVector( files );
    std::vector< std::string > files_path;
    // to ensure no additional time measured by creating filename strings
    for ( auto &file : files ) {
        files_path.push_back( path + "/" + file );
    }

    auto result = 0;
    auto start = std::chrono::steady_clock::now();

    for ( auto &file : files_path ) {
        result = FSLib::remove( file );
        if ( result != 0 ) {
            return LDBL_MAX;
        }
    }

    auto end = std::chrono::steady_clock::now();

    long double average =
        std::chrono::duration_cast< std::chrono::nanoseconds >( end - start )
            .count();

    average /= files.size();
    // average in seconds
    average /= 1000000000;

    return 1.0 / average;
}

long double creationBenchmark( std::vector< std::string > & /*UNUSED*/,
                               uint64_t /*UNUSED*/, uint64_t file_count,
                               const std::string &path ) {
    // allocate memory for path, "/file_" and maximum number of digits possible
    // for `file_count` and ending 0
    char *file_string =
        new char[path.size() + 7 + std::to_string( file_count ).size()];
    std::strcpy( file_string, path.c_str() );
    std::strcpy( file_string + path.size(), "/file_" );
    // store position where to write file number
    auto *file_number_pos = file_string + path.size() + 6;

    auto start = std::chrono::steady_clock::now();

    for ( uint64_t i = 0; i < file_count; i++ ) {
        // write file's number to file_string
        sprintf( file_number_pos, "%lu", i );
        // open file for creation only
        auto fd_to = open( file_string, O_CREAT, 0644 );

        if ( fd_to < 0 ) {
            LOG( "Couldn't open file " + std::string( file_string ) );
            LOG( strerror( errno ) );
            return LDBL_MAX;
        }

        if ( close( fd_to ) < 0 ) {
            LOG( "Couldn't close file " + std::string( file_string ) );
            LOG( strerror( errno ) );
            return LDBL_MAX;
        }
    }

    auto end = std::chrono::steady_clock::now();

    // free allocated memory
    delete[] file_string;

    // return difference between start and end in nanoseconds
    long double average =
        std::chrono::duration_cast< std::chrono::nanoseconds >( end - start )
            .count();

    average /= file_count;
    // average in seconds
    average /= 1000000000;

    return 1.0 / average;
}

// clean up files after move benchmark
bool cleanUpMoved( const std::vector< std::string > &files,
                   const std::string &path ) {
    for ( const auto &x : files ) {
        FSLib::move( path + "/moved_" + x, path + "/" + x );
    }
    return true;
}

// clean up files after move benchmark (multiple threads)
bool cleanUpMoved( const std::vector< std::vector< std::string > > &files,
                   const std::string &path ) {
    for ( uint64_t i = 0; i < files.size(); i++ ) {
        cleanUpMoved( files[i], path + "/thread" + std::to_string( i ) );
    }
    return true;
}

bool noCleanUp( const std::vector< std::string > & /*UNUSED*/,
                const std::string & /*UNUSED*/ ) {
    return true;
}
