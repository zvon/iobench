#include "FSLib/utilities.hpp"
#include "functions.hpp"
#include <iomanip>
#include <iostream>

int main( int argc, char **argv ) {
    uint64_t file_size{};
    uint64_t file_count{};
    uint64_t creation_file_count{ 1000000 };
    int threads{ 1 };
    std::string benchmarks{};
    std::string path{ "." };
    std::vector< std::string > nodes;
    bool ssh{ false };
    bool delete_files{ true };

    if ( argc == 1 ) {
        printHelp();
        return 0;
    }

    auto cmd = parseCommandLine( argc, argv, file_size, file_count,
                                 creation_file_count, threads, benchmarks, ssh,
                                 delete_files, nodes, path );

    if ( cmd == PARSE_ERROR )
        return 1;
    else if ( cmd == PARSE_HELP )
        return 0;

    // if file_size is not set or file_count is not set
    // or no benchmarks are set and this isn't an ssh client
    // and the only benchmark is not creation, return error
    if ( ( ( file_size == 0 || file_count == 0 ) && benchmarks != "c" ) ||
         ( benchmarks.size() == 0 && !ssh ) )
        return 1;

    // set output precision
    std::cout << std::fixed << std::setprecision( 3 );

    // don't reset ssh log file because of NODE # header
    if ( !ssh )
        resetLOG();

    if ( !nodes.empty() ) {
        // run benchmark on given nodes
        if ( !nodeBenchmark( benchmarks, nodes, file_count, file_size,
                             creation_file_count, threads, delete_files, path ) )
            return 1;
    } else if ( !ssh ) {
        // run benchmark on local machine
        localBenchmark( benchmarks, file_count, file_size, creation_file_count,
                        threads, delete_files, path );
    } else {
        // run benchmark as a node and report result to server
        sshBenchmark( file_count, file_size, creation_file_count, threads,
                      delete_files, path );
    }
}
